# -*- coding: utf-8 -*-
"""Sudoku.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1PwprQAwdY-l0NwMryBr750-n0jkf-20C
"""

## Lo primero que hicimos fue proponer el tablero.

## Cuando lo estabamos haciendo en c++ primero llenamos una matriz con ceros.

tablero = [
    [7,8,0,4,0,0,1,2,0],
    [6,0,0,0,7,5,0,0,9],
    [0,0,0,6,0,1,0,7,8],
    [0,0,7,0,4,0,2,6,0],
    [0,0,1,0,5,0,9,3,0],
    [9,0,4,0,6,0,0,0,5],
    [0,7,0,3,0,0,0,1,2],
    [1,2,0,0,0,7,4,0,0],
    [0,4,9,2,0,6,0,0,7]
]

## Aqui implementamos la funcion que permita al final del codigo en ejecucion visualizar el tablero.

def ver_tablero(tablero):

    for i in range(len(tablero)):

        if i % 3 == 0 and i != 0:

            print("- - - - - - - - - - - - - ")

        for j in range(len(tablero[0])):

            if j % 3 == 0 and j != 0:

                print(" | ", end="")

            if j == 8:

                print(tablero[i][j])

            else:

                print(str(tablero[i][j]) + " ", end="")
'''
 Un problema que teniamos frecuentemente tanto en Python como en C++ al momento de hacer el codigo 
 fue lo principal ordenar los numeros del tablero, pero tambien los espacios vacios.

'''

## Por lo que creamos la funcion que nos permitia solucionar el inconveniente.

def encontrar_v(tablero):

    for i in range(len(tablero)):

        for j in range(len(tablero[0])):

            if tablero[i][j] == 0:

                return i, j

    return None

'''

Un problema frecuente al principio de nuestro tabrajo fue el como hacer el sudoku como tal, ya que

ninguno de nosotros habia resuleto uno o incluso jugarlo, asi que investigamos el funcionamiento del juego 

y creamos la funcion para resolverlo.

'''


def resolver(tablero):

    encontrar = encontrar_v(tablero)

    if not encontrar:

        return True

    else:

        fila, columna = encontrar

    for i in range(1, 10):

        if valida(tablero, i, (fila, columna)):

            tablero[fila][columna] = i

            if resolver(tablero):

                return True

            tablero[fila][columna] = 0

    return False

''' 
Junto con la funcion que nos permitia realizar la solucion del juego con el tablero propuesto, debiamos

condicionar el juego como un "if", pero lo hicimos con una funcion para validar el procedimiento.

'''





def valida(tablero, numero, posicion):

    if (checar_fila(tablero, numero, posicion) and

            checar_columna(tablero, numero, posicion) and

            checar_c(tablero, numero, posicion)):

        return True

    return False

''' 

Con las funciones:

def_checar_fila.

def_checar_columna.

def checar_c.

hacemos validaciones y lo tratamos de hacer como en C++, tratando estas dos funciones 

como si estuvieramos trabajando una matriz y validandola.

Aqui lo quise recordar como validar con el tema de busqueda en un arbol binario 

regresar un False o un True y hacer semejantes las funciones como en los programas que hicimos.

'''


def checar_fila(tablero, numero, posicion):

    for i in range(len(tablero[0])):

        if tablero[posicion[0]][i] == numero and posicion[1] != i:

            return False

    return True


def checar_columna(tablero, numero, posicion):

    for i in range(len(tablero)):

        if tablero[i][posicion[1]] == numero and posicion[0] != i:

            return False

    return True


def checar_c(tablero, numero, posicion):

    c_x = posicion[1] // 3

    c_y = posicion[0] // 3

    for i in range(c_y * 3, c_y * 3 + 3):

        for j in range(c_x * 3, c_x * 3 + 3):

            if tablero[i][j] == numero and (i, j) != posicion:

                return False

    return True


''' 

Aqui se ejecuta todo el programa.

'''


#--------------------------MAIN

ver_tablero(tablero)

resolver(tablero)

print("____________")

ver_tablero(tablero)